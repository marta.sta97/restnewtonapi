# REST Newton API App

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Installation](#installation)

## General info
Mobile app that uses REST API for simple math calculations.

## Technologies
Project is created with:
* Kotlin version: 1.3.21
* [Retrofit](https://github.com/square/retrofit) version: 2.5.0
* [Newton Api](https://github.com/aunyks/newton-api)

## Installation
1. Download this project as zip and extract it
2. Import it in Android Studio
3. Sync Gradle and run on your device/emulator

