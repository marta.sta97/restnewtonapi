package com.codingdragons.restnewtonapi

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import android.view.inputmethod.InputMethodManager

class MainActivity : AppCompatActivity() {

    private lateinit var newtonApi: NewtonAPI

    companion object {
        const val TAG = "cd2019"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val retrofit = Retrofit.Builder()
            .baseUrl("https://newton.now.sh")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        newtonApi = retrofit.create(NewtonAPI::class.java)

        button0.setOnClickListener {
            val expression = expressionEditText.text.toString()
            if (expression.isNotEmpty()) {
                getListResponse(expression)
            }
            closeKeyboard()
        }

        for (i in 1 until 15) {
            val resID = resources.getIdentifier("button$i", "id", packageName)
            val button = findViewById<Button>(resID)
            button.setOnClickListener {
                val expression = expressionEditText.text.toString()
                if (expression.isNotEmpty()) {
                    getResponse(button.text.toString(), expression)
                }
                closeKeyboard()
            }
        }

        resultView.text = getString(R.string.result, "")

    }

    private fun getResponse(operation: String, expression: String) {
        val call = newtonApi.response(operation, expression)

        call.enqueue(object : Callback<NewtonDTO> {
            override fun onFailure(call: Call<NewtonDTO>, t: Throwable) {
                Log.d(TAG, "error")
            }

            override fun onResponse(call: Call<NewtonDTO>, response: Response<NewtonDTO>) {
                val body = response.body()
                val resp = if (body!!.operation.isNullOrEmpty())
                    getString(R.string.unable_to_perform_calculation)
                else
                    body.result

                setResponse(resp)
            }
        })
    }

    private fun getListResponse(expression: String) {
        val call = newtonApi.listResponse(expression)

        call.enqueue(object : Callback<NewtonListDTO> {
            override fun onFailure(call: Call<NewtonListDTO>, t: Throwable) {
                Log.d(TAG, "error")
            }

            override fun onResponse(call: Call<NewtonListDTO>, response: Response<NewtonListDTO>) {
                val body = response.body()
                val resp = if (body!!.operation.isNullOrEmpty())
                    getString(R.string.unable_to_perform_calculation)
                else
                    body.result.toString()
                setResponse(resp)
            }
        })
    }

    fun setResponse(response: String) {
        resultView.text = getString(R.string.result, response)
    }

    private fun closeKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

}
