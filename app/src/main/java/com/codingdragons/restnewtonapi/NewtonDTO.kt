package com.codingdragons.restnewtonapi

data class NewtonDTO(
    var operation: String,
    var expression: String,
    var result: String
)

data class NewtonListDTO(
    var operation: String,
    var expression: String,
    var result: List<String>
)