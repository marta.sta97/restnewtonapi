package com.codingdragons.restnewtonapi

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface NewtonAPI {

    @GET("/{operation}/{expression}")
    fun response(@Path("operation") operation: String, @Path("expression") expression: String): Call<NewtonDTO>

    @GET("/zeroes/{expression}")
    fun listResponse(@Path("expression") expression: String): Call<NewtonListDTO>

}